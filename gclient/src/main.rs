use std::time::Instant;

use grpc::dbapi::db_api_client::DbApiClient;
use grpc::dbapi::{GetRequest, UpsertRequest};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut client = DbApiClient::connect("http://0.0.0.0:4564").await?;

    let key = "/abc/heya";

    let upsert_req = tonic::Request::new(UpsertRequest {
        key : key.into(),
        value : "ello guvnor".into(),
    });
    client.upsert(upsert_req).await?;

    let request = tonic::Request::new(GetRequest {
        key: key.into(),
    });

    let now = Instant::now();
    let response = client.get(request).await?;
    println!("{}", now.elapsed().as_micros());

    println!("RESPONSE={:?}", response);

    Ok(())
}
