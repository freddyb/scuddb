use axum::{
    extract::{Extension, Path, TypedHeader},
    http::header::{self, HeaderValue},
    response::{AppendHeaders, IntoResponse, Redirect},
    routing::{get, get_service, post},
    Router,
};

use tower::ServiceBuilder;
use tower_http::{set_header::SetResponseHeaderLayer, trace::TraceLayer};

use headers::Cookie;

use uuid::Uuid;

use rpc::ScudProtocolClient;
use std::error::Error;
use std::net::SocketAddr;
use tarpc::{client, context, tokio_serde::formats::Json};

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();

    let server_addr = parse_addr("0.0.0.0:4563");

    let client = get_client(server_addr).await.unwrap();

    let hostname: String = std::env::args().nth(1).unwrap();

    let app = Router::new()
        .route(
            "/paymentrequest",
            get_service(tower_http::services::ServeFile::new("src/index.html")).handle_error(
                |e| async move {
                    (
                        axum::http::StatusCode::INTERNAL_SERVER_ERROR,
                        format!("Unhandled internal error: {}", e),
                    )
                },
            ),
        )
        .route("/login", post(login))
        .route("/create_user", post(create_user))
        .route("/balance", get(user_balance))
        .route("/pay/:payment_to/:amount", post(pay))
        .layer(
            ServiceBuilder::new()
                .layer(axum::Extension(client))
                .layer(axum::Extension(hostname))
                .layer(TraceLayer::new_for_http())
                .layer(SetResponseHeaderLayer::<_>::overriding(
                    header::ACCESS_CONTROL_ALLOW_ORIGIN,
                    HeaderValue::from_static("http://localho.st:3010"),
                ))
                .layer(SetResponseHeaderLayer::<_>::overriding(
                    header::ACCESS_CONTROL_ALLOW_CREDENTIALS,
                    HeaderValue::from_static("true"),
                )),
        );

    axum::Server::bind(&"0.0.0.0:3030".parse().unwrap())
        .serve(app.into_make_service())
        .await
        .unwrap();
}

async fn login(body: String) -> impl IntoResponse {
    let uuid = Uuid::parse_str(&body).unwrap();
    let headers = AppendHeaders([(
        header::SET_COOKIE,
        format! {"user_id={}; SameSite=Lax;", uuid},
    )]);
    return (headers, format! {"logged in user {}", uuid});
}

async fn create_user(
    Extension(client): Extension<rpc::ScudProtocolClient>,
) -> Result<String, (axum::http::StatusCode, String)> {
    let uuid = Uuid::new_v4().to_string();
    //the as &str is here because of the following compiler bug:
    //https://github.com/rust-lang/rust/issues/92178
    let uuid_prefix = String::from("/") + &uuid as &str;
    //this would be much faster with tarpc request pipelining
    client
        .create_prefix(context::current(), uuid_prefix.clone() + "/")
        .await
        .unwrap()
        .map_err(|e| (axum::http::StatusCode::BAD_GATEWAY, e.to_string()))?;
    client
        .upsert(
            context::current(),
            uuid_prefix + "/balance",
            "10".to_string(),
        )
        .await
        .unwrap()
        .map_err(|e| (axum::http::StatusCode::BAD_GATEWAY, e.to_string()))?;
    Ok(uuid)
}

async fn user_balance(
    Extension(client): Extension<rpc::ScudProtocolClient>,
    TypedHeader(cookie): TypedHeader<Cookie>,
) -> String {
    let user_id = match cookie.get("user_id") {
        Some(user_id) => user_id,
        None => return String::from("not logged in"),
    };
    let balance = get_user_balance(&client, user_id).await.to_string();
    return format! {"balance for user {} is {}", user_id, balance};
}

async fn pay(
    Extension(client): Extension<rpc::ScudProtocolClient>,
    TypedHeader(cookie): TypedHeader<Cookie>,
    Path((payment_to, amount)): Path<(Uuid, u64)>,
    Extension(hostname): Extension<String>,
) -> Result<impl IntoResponse, impl IntoResponse> {
    let user_id = match cookie.get("user_id") {
        Some(user_id) => user_id,
        None => {
            return Err((
                axum::http::StatusCode::UNAUTHORIZED,
                String::from("not logged in"),
            ))
        }
    };

    let balance = get_user_balance(&client, user_id).await;
    if balance < amount {
        return Err((axum::http::StatusCode::PAYMENT_REQUIRED, String::from("insufficient balance"),))
    }
    let new_balance = balance - amount;

    client.compare_and_swap(context::current(), String::from("/") + user_id + "/balance", Some(balance.to_string()), Some(new_balance.to_string())).await.unwrap().unwrap();

    Ok(Redirect::to(&format!("http://{}:3000/paidfor/123", hostname)))
}

async fn get_client(server_addr: SocketAddr) -> Result<ScudProtocolClient, Box<dyn Error>> {
    let mut transport = tarpc::serde_transport::tcp::connect(server_addr, Json::default);
    transport.config_mut().max_frame_length(4294967296);
    Ok(rpc::ScudProtocolClient::new(client::Config::default(), transport.await?).spawn())
}

fn parse_addr(socket_string: &str) -> SocketAddr {
    socket_string
        .parse::<SocketAddr>()
        .unwrap_or_else(|e| panic!(r#"--server_addr value "{}" invalid: {}"#, socket_string, e))
}

async fn get_user_balance(client: &rpc::ScudProtocolClient, user_id: &str) -> u64 {
    client
        .get(context::current(), String::from("/") + user_id + "/balance")
        .await
        .unwrap()
        .unwrap()
        .unwrap().parse().unwrap()
}
