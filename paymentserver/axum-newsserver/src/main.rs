use axum::{
    extract::{Extension, Path},
    http::header::{self, HeaderValue},
    routing::{get, get_service},
    Router,
};

use tower::ServiceBuilder;
use tower_http::{set_header::SetResponseHeaderLayer, trace::TraceLayer};

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();

    let hostname: String = std::env::args().nth(1).unwrap();

    let app = Router::new()
        .route("/", get(root))
        .route(
            "/paidfor/:payment_token",
            get_service(tower_http::services::ServeFile::new("src/news.html")).handle_error(
                |e| async move {
                    (
                        axum::http::StatusCode::INTERNAL_SERVER_ERROR,
                        format!("Unhandled internal error: {}", e),
                    )
                },
            ),
        )
        .layer(
            ServiceBuilder::new()
                .layer(axum::Extension(hostname))
                .layer(TraceLayer::new_for_http())
                .layer(SetResponseHeaderLayer::<_>::overriding(
                    header::ACCESS_CONTROL_ALLOW_ORIGIN,
                    HeaderValue::from_static("*"),
                )),
        );

    // run it with hyper on localhost:3000
    axum::Server::bind(&"0.0.0.0:3000".parse().unwrap())
        .serve(app.into_make_service())
        .await
        .unwrap();
}

async fn root(Extension(hostname): Extension<String>) -> axum::response::Redirect {
    return axum::response::Redirect::to(&format!("http://{}:3030/paymentrequest?payment_to=9ade8407-cf3b-4585-bcc5-561ce2dde203&amount=1", hostname));
}

//async fn paidfor(Path(payment_token): Path<String>) -> String {
//    return String::from("the news today is that carrots come in colors besides orange");
//}
