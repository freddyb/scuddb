use warp::http::header::{HeaderMap, HeaderValue};
use warp::Filter;

#[tokio::main]
async fn main() {
    let mut headers = HeaderMap::new();
    headers.insert(
        "Set-Cookie",
        HeaderValue::from_static("paymentid=mkdqsfjflqs; SameSite=None; Secure"),
    );

    let create_user_route = warp::post()
        .and(warp::path!("users" / usize))
        .and_then(create_user);

    let balance_route = warp::get()
        .and(warp::path!("users" / usize / "balance"))
        .map(|userid: usize| format!("balance for user {} is X", userid));

    let transfer_route = warp::post()
        .and(warp::path!("transfer" / usize / usize / usize))
        .map(|useridfrom: usize, useridto: usize, amount: usize| {
            format!(
                "paid {} from user {} to user {}",
                amount, useridfrom, useridto
            )
        })
        .with(warp::reply::with::headers(headers));

    let routes = create_user_route.or(balance_route).or(transfer_route);

    warp::serve(routes).run(([127, 0, 0, 1], 3030)).await;
}

async fn create_user(userid: usize) -> Result<String, std::convert::Infallible> {
    Ok(format!("created user {}", userid))
}
