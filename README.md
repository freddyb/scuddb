# ScudDB
### A strongly consistent distributed database for edge compute.

For my masters thesis I’ve created ScudDB, a distributed database, which aims to provide
strongly consistent data storage for some edge compute applications.
It achieves this by giving specific nodes authority over a certain prefix in the database.
These nodes can then service reads, writes and create new sub-prefixes without having to communicate
with any other nodes. For example, one node might have authority over “/”, another 
over “/students/” and another over “/professors/”. 
These nodes then oversee the creation of new sub-prefixes on other nodes, such 
as “/students/Bob/” and “Professors/John/” respectively.

This model also has attractive failure tolerance properties since there’s no centralized entity responsible for prefix distribution or request routing.
There is only one type of node, simplifying the system architecture.

Clients can query the entire database through a single database node because requests are forwarded to the appropriate node.
However, for request forwarding, traveling up the entire prefix hierarchy and back down to the correct node is slow.
To solve this problem, I used libp2p’s gossipsub to spread metadata about which prefixes are stored where.
Compared to using a classic pubsub system for this purpose, libp2p lets me simplify the architecture of the entire system, without introducing another point of failure.

## Implementation
- [Sled](https://sled.rs/) for local storage
- [Tarpc](https://github.com/google/tarpc) for RPC calls (grpc is available for some methods)
- [Libp2p](https://github.com/libp2p/rust-libp2p) to share metadata between nodes about which nodes own which prefixes

## Repo structure
- The rpc directory contains the RPC method definitions. There's an incomplete grpc equivalent.
- The server directory contains the database Implementation
- The client and gclient directories contain example clients. These are not client libraries.
- The paymentserver directory contains a very rudimentary micropayments test case.

ScudDB is named after a [low to the ground cloud](https://en.wikipedia.org/wiki/Scud_(cloud)).
