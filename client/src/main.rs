use itertools::Itertools;
use rand::{distributions::Alphanumeric, thread_rng, Rng};
use rpc::ScudProtocolClient;
use std::env;
use std::error::Error;
use std::fs::{self, File};
use std::io::prelude::*;
use std::net::ToSocketAddrs;
use std::time::{Duration, Instant};
use std::{io, net::SocketAddr};
use tarpc::{client, context, tokio_serde::formats::Json};
use tokio::task::JoinSet;
use grpc::dbapi::db_api_client::DbApiClient;
use grpc::dbapi::{GetRequest, UpsertRequest};

#[tokio::main]
async fn main() -> io::Result<()> {
    let server_addr_root = parse_addr(&env::args().nth(1).expect("pass a valid server socketaddr"));
    let server_addr_second = parse_addr(
        &env::args()
            .nth(2)
            .expect("pass a second valid server socketaddr"),
    );
    dbg!(&server_addr_root);
    dbg!(&server_addr_second);

    let root_client = get_client(server_addr_root).await.unwrap();
    let second_client = get_client(server_addr_second).await.unwrap();

    let request_spacing = Duration::from_micros(24000);

    test_ping(&root_client).await;
    test_ping(&second_client).await;
    test_simple_upsert_get(&second_client).await;
    //test_get(&second_client, request_spacing).await;
    //test_forwarding_get(&root_client, &second_client, request_spacing).await;
    //test_mixed_forwarding_get(&root_client, &second_client, request_spacing).await;
    //test_prefix_creation(&root_client, request_spacing).await;
    //test_forwarding_prefix_creation(&root_client, &second_client, request_spacing).await;
    
    test_get_grpc(SocketAddr::new(server_addr_root.ip(), server_addr_root.port() +1), request_spacing).await;

    Ok(())
}

async fn test_ping(client: &ScudProtocolClient) {
    client.ping(context::current()).await.unwrap();
}

async fn test_simple_upsert_get(client: &ScudProtocolClient) {
    let key: String = "/helloa".to_string();
    //upsert value
    let result = client
        .upsert(context::current(), key.clone(), "ello".to_string())
        .await;

    dbg!(result).unwrap().unwrap();

    let value = client.get(context::current(), key.clone()).await;
    dbg!(value).unwrap().unwrap();
}

async fn test_get(client: &ScudProtocolClient, duration: Duration) {
    let key = "/alloab/lfjsk";
    client
        .create_prefix(context::current(), "/alloab/".to_string())
        .await
        .unwrap()
        .ok();
    client
        .upsert(
            context::current(),
            key.to_string(),
            "alloa_value".to_string(),
        )
        .await
        .unwrap()
        .unwrap();

    let now = Instant::now();

    let client = client.clone();
    let mut futs = tokio::task::spawn_blocking(move || {
        let mut futs = JoinSet::new();
        for _ in 0..100_000 {
            let client = client.clone();
            futs.spawn(async move {
                let now2 = Instant::now();
                client
                    .clone()
                    .get(context::current(), key.to_string())
                    .await
                    .unwrap()
                    .unwrap()
                    .unwrap();
                now2.elapsed().as_micros()
            });
            spin_sleep::sleep(duration);
        }
        futs
    })
    .await
    .unwrap();

    let mut resultingvec: Vec<u128> = Vec::new();

    while let Some(t) = futs.join_one().await.unwrap() {
        resultingvec.push(t);
    }

    println!("test_get: {}", now.elapsed().as_millis());
    vec_to_file(resultingvec, "get");
}

async fn test_forwarding_get(
    client_one: &ScudProtocolClient,
    client_two: &ScudProtocolClient,
    duration: Duration,
) {
    let key = "/alboa/lfjsk";
    client_one
        .create_prefix(context::current(), "/alboa/".to_string())
        .await
        .unwrap()
        .ok();
    client_two
        .upsert(
            context::current(),
            key.to_string(),
            "alloa_value".to_string(),
        )
        .await
        .unwrap()
        .unwrap();
    let now = Instant::now();
    let client_two = client_two.clone();
    let mut futs = tokio::task::spawn_blocking(move || {
        let mut futs = JoinSet::new();
        for _ in 0..100_000 {
            let client_two = client_two.clone();
            futs.spawn(async move {
                let now2 = Instant::now();
                client_two
                    .get(context::current(), key.to_string())
                    .await
                    .unwrap()
                    .unwrap()
                    .unwrap();
                now2.elapsed().as_micros()
            });
            spin_sleep::sleep(duration);
        }
        futs
    })
    .await
    .unwrap();

    let mut timevec: Vec<u128> = Vec::new();

    while let Some(t) = futs.join_one().await.unwrap() {
        timevec.push(t);
    }

    println!("test_forwarding_get: {}", now.elapsed().as_millis());
    vec_to_file(timevec, "forwarding_get");
}

async fn test_mixed_forwarding_get(
    client_one: &ScudProtocolClient,
    client_two: &ScudProtocolClient,
    duration: Duration,
) {
    let forwarded_key = "/albia/lfjsk";
    client_one
        .create_prefix(context::current(), "/albia/".to_string())
        .await
        .unwrap()
        .ok();
    client_two
        .upsert(
            context::current(),
            forwarded_key.to_string(),
            "alloa_value".to_string(),
        )
        .await
        .unwrap()
        .unwrap();

    let local_key = "/abuelita/mjoiaeg/";
    client_two
        .create_prefix(context::current(), "/abuelita/".to_string())
        .await
        .unwrap()
        .ok();
    client_two
        .upsert(
            context::current(),
            local_key.to_string(),
            "local value".to_string(),
        )
        .await
        .unwrap()
        .unwrap();
    let now = Instant::now();
    let client_two = client_two.clone();
    let mut futs = tokio::task::spawn_blocking(move || {
        let mut futs = JoinSet::new();
        for i in 0..100_000 {
            let client_two = client_two.clone();
            futs.spawn(async move {
                let now2 = Instant::now();
                if i % 2 == 0 {
                    client_two
                        .get(context::current(), forwarded_key.to_string())
                        .await
                        .unwrap()
                        .unwrap()
                        .unwrap();
                } else {
                    client_two
                        .get(context::current(), local_key.to_string())
                        .await
                        .unwrap()
                        .unwrap()
                        .unwrap();
                }
                now2.elapsed().as_micros()
            });
            spin_sleep::sleep(duration);
        }
        futs
    })
    .await
    .unwrap();

    let mut timevec: Vec<u128> = Vec::new();
    while let Some(t) = futs.join_one().await.unwrap() {
        timevec.push(t);
    }
    println!("test_mixed_forwarding_get: {}", now.elapsed().as_millis());
    vec_to_file(timevec, "mixed_get");
}

async fn test_prefix_creation(client: &ScudProtocolClient, duration: Duration) {
    client
        .create_prefix(context::current(), "/bolaba/".to_string())
        .await
        .unwrap()
        .ok();
    let loopclient = client.clone();
    let now = Instant::now();
    let mut futs = tokio::task::spawn_blocking(move || {
        let prefix_chunks = thread_rng().sample_iter(Alphanumeric).take(90000).chunks(9);
        let prefix_iter = prefix_chunks
            .into_iter()
            .map(|c| String::from_utf8_lossy(&c.collect::<Vec<u8>>()).to_string());
        let mut futs = JoinSet::new();
        for prefix in prefix_iter {
            let loopclient = loopclient.clone();
            futs.spawn(async move {
                let now2 = Instant::now();
                loopclient
                    .create_prefix(context::current(), format!("/bolaba/{prefix}/"))
                    .await
                    .unwrap()
                    .unwrap();
                now2.elapsed().as_millis()
            });
            spin_sleep::sleep(duration);
        }
        futs
    })
    .await
    .unwrap();

    let mut timevec: Vec<u128> = Vec::new();
    while let Some(t) = futs.join_one().await.unwrap() {
        timevec.push(t);
    }
    println!("test_prefix_creation: {}", now.elapsed().as_millis());
    vec_to_file(timevec, "prefix");
    
    client
        .create_prefix(context::current(), "/bolaba/end/".to_string())
        .await
        .unwrap()
        .ok();
    println!("done creating end prefix");
}

async fn test_forwarding_prefix_creation(
    client_one: &ScudProtocolClient,
    client_two: &ScudProtocolClient,
    duration: Duration,
) {
    client_one
        .create_prefix(context::current(), "/preftest/".to_string())
        .await
        .unwrap()
        .unwrap();
    let client_two = client_two.clone();
    let mut futs = tokio::task::spawn_blocking(move || {
        let prefix_chunks = thread_rng().sample_iter(Alphanumeric).take(9000).chunks(9);
        let prefix_iter = prefix_chunks
            .into_iter()
            .map(|c| String::from_utf8_lossy(&c.collect::<Vec<u8>>()).to_string());
        let now = Instant::now();
        let mut futs = JoinSet::new();
        for prefix in prefix_iter {
            let client_two = client_two.clone();
            futs.spawn(async move {
                let now2 = Instant::now();
                client_two
                    .create_prefix(context::current(), format!("/preftest/{prefix}/"))
                    .await
                    .unwrap()
                    .unwrap();
                now2.elapsed().as_millis()
            });
            spin_sleep::sleep(duration);
        }
        println!(
            "test_forwarding_prefix_creation: {}",
            now.elapsed().as_millis()
        );
        futs
    })
    .await
    .unwrap();

    let mut timevec: Vec<u128> = Vec::new();
    while let Some(t) = futs.join_one().await.unwrap() {
        timevec.push(t);
    }
    vec_to_file(timevec, "prefix_forwarded");
}

async fn test_get_grpc(server_addr: SocketAddr, duration: Duration) {
    let mut client = DbApiClient::connect(format!("http://{server_addr}")).await.unwrap();

    let key = "/abc/heya";

    let upsert_req = tonic::Request::new(UpsertRequest {
        key : key.into(),
        value : "ello guvnor".into(),
    });
    client.upsert(upsert_req).await.unwrap();
    
    let now = Instant::now();

    let client = client.clone();
    let mut futs = tokio::task::spawn_blocking(move || {
        let mut futs = JoinSet::new();
        for _ in 0..1_000 {
            let mut client = client.clone();
            let get_req = tonic::Request::new(GetRequest {
                key: key.into(),
            });
            futs.spawn(async move {
                let now2 = Instant::now();
                client.get(get_req).await.unwrap();
                now2.elapsed().as_micros()
            });
            spin_sleep::sleep(duration);
        }
        futs
    })
    .await
    .unwrap();

    let mut resultingvec: Vec<u128> = Vec::new();

    while let Some(t) = futs.join_one().await.unwrap() {
        resultingvec.push(t);
    }

    println!("test_get: {}", now.elapsed().as_millis());
    vec_to_file(resultingvec, "get_grpc");
}

fn parse_addr(socket_string: &str) -> SocketAddr {
    get_socket_addr(socket_string)
}

async fn get_client(server_addr: SocketAddr) -> Result<rpc::ScudProtocolClient, Box<dyn Error>> {
    let mut transport = tarpc::serde_transport::tcp::connect(server_addr, Json::default);
    transport.config_mut().max_frame_length(4294967296);
    Ok(rpc::ScudProtocolClient::new(client::Config::default(), transport.await?).spawn())
}

fn vec_to_file(mut vec: Vec<u128>, name: &str) {
    vec.sort_unstable();
    fs::create_dir_all("output").expect("failed making output directory");
    let mut vecfile = File::create(format!("output/{name}_vecfile_us.csv")).unwrap();
    let stringvec: Vec<String> = vec.iter().map(|n| n.to_string()).collect();
    write!(vecfile, "{}", stringvec.join("\n")).unwrap();
}

pub fn get_socket_addr(string_addr: &str) -> SocketAddr {
    string_addr.to_socket_addrs().unwrap().next().unwrap()
}
