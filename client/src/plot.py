from collections import Counter
import matplotlib.pyplot as plt
import csv
import numpy as np
import sys
import re

plt.rcParams.update({'font.size': 18})

timing_list = []
label_list = []

for filename in sys.argv[1:]:
    print(filename)
    with open(filename,'r') as plot_csv:
        plots = csv.reader(plot_csv, delimiter=',')
        intlist = [int(l[0]) for l in plots]
    timing_list.append([i for i in intlist])
    label_list.append(re.split("/", filename)[-2])
    timecount = Counter((i for i in intlist))
    maxlatency = max(k for k, v in timecount.items())
    plt.bar(list(timecount.keys()), list(timecount.values()), align='center')
    plt.xlabel("latency (µs)")
    plt.ylabel("amount of requests")
    plt.title(f"latency histogram of sending 100 000 requests at {label_list[-1]} requests/s");
    plt.ylim(ymin=0)
    plt.xlim(xmin=0)
    plt.show()
    plt.clf()
    print(np.percentile(intlist, 90))

plt.xlabel("requests per second")
plt.ylabel("latency (µs)")
plt.title("latency box plot of sending 100 000 requests")
plt.boxplot(timing_list, labels = label_list, showfliers = False)
plt.show()

