use bincode::deserialize;
use libp2p::core::PeerId;
use libp2p::Multiaddr;
use sled;
use std::net::SocketAddr;
use thiserror::Error;
use tonic::Status;

#[tarpc::derive_serde]
#[derive(Debug, Error)]
pub enum ScudError {
    #[error("Operation failed")]
    OperationFailed,
    #[error("Prefix {0} Invalid")]
    InvalidPrefix(String),
    #[error("Sub key {1} already exists under prefix {0}")]
    SubKeyExists(String, String),
    #[error("Sub prefix {1} already exists under prefix {0}")]
    SubPrefixExists(String, String),
    #[error("Swapping value failed, expected {0} but actual value was {1}")]
    CompareAndSwapError(String, String),
}

impl From<ScudError> for String {
    fn from(e: ScudError) -> String {
        e.to_string()
    }
}

impl From<sled::CompareAndSwapError> for ScudError {
    fn from(
        sled::CompareAndSwapError { current, proposed }: sled::CompareAndSwapError,
    ) -> ScudError {
        let current_string = current.map_or(String::from("no value"), |ivec| {
            format!("\"{}\"", deserialize::<String>(&ivec).unwrap())
        });
        let proposed_string = proposed.map_or(String::from("no value"), |ivec| {
            format!("\"{}\"", deserialize::<String>(&ivec).unwrap())
        });
        ScudError::CompareAndSwapError(current_string, proposed_string)
    }
}

impl From<ScudError> for Status {
    fn from(e: ScudError) -> Status {
        match e {
            ScudError::InvalidPrefix(_) => Status::not_found(e),
            ScudError::OperationFailed => Status::internal(e),
            ScudError::SubKeyExists(_, _) | ScudError::SubPrefixExists(_, _) => {
                Status::failed_precondition(e)
            }
            //temporary
            _ => Status::internal(e),
        }
    }
}

///Represents a reference to a newly created or moved prefix. Contains the prefix and address of
///its authoritative node.
#[tarpc::derive_serde]
#[derive(Debug)]
pub struct PrefixNode {
    pub prefix: String,
    pub address: std::net::SocketAddr,
    pub version: u32,
}

#[tarpc::service]
pub trait ScudProtocol {
    ///Pings the local node
    async fn ping() -> ();
    ///Gets a single key
    async fn get(key: String) -> Result<Option<String>, ScudError>;
    ///Get multiple prefixes. These prefixes must all reside under the same prefix
    async fn get_multiple(keys: Vec<String>) -> Result<Vec<Option<String>>, ScudError>;
    ///Upserts a key. Guaranteed to be durable.
    async fn upsert(key: String, value: String) -> Result<(), ScudError>;
    ///Compares and swaps a key/value pair. Guaranteed to be durable.
    async fn compare_and_swap(
        key: String,
        old: Option<String>,
        new: Option<String>,
    ) -> Result<(), ScudError>;
    ///Get multiple prefixes. These prefixes must all reside under the same prefix
    //async fn compare_and_swap_multiple(keys: Vec<(String, String, String)>) -> Result<Vec<Option<String>>, ScudError>;
    ///Register a prefix with the current node. Guaranteed to be durable
    async fn create_prefix(prefix: String) -> Result<(), ScudError>;
    ///Only for use by other database nodes. In the future a separate API may be created for this.
    async fn upsert_prefix_ref(prefix: String, addr: SocketAddr) -> Result<(), ScudError>;
    ///Used by a subnode when requesting the creation of a sub-prefix.
    ///Upserting a prefix ref is not allowed if another sub-prefix or value
    ///already exists.
    async fn request_prefix_creation(prefix: String, addr: SocketAddr) -> Result<(), ScudError>;
    ///To get the multiaddr and PeerId of another node. Only for use by other database nodes.
    async fn get_p2p_info() -> Result<(Multiaddr, PeerId), ScudError>;
    ///Only for use by other database nodes. Aids peer discovery on startup & gets existing prefix
    ///database
    async fn get_prefixes() -> Result<Vec<PrefixNode>, ScudError>;
}
