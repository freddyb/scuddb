use bincode::serialize;
use libp2p::PeerId;
use std::net::IpAddr;
use std::net::SocketAddr;

use tokio::sync::mpsc::{self, Receiver, Sender};

use libp2p::gossipsub::IdentTopic;
use libp2p::identity::Keypair;
use libp2p::multiaddr::Protocol;
use libp2p::Multiaddr;
use libp2p::Swarm;

use futures::StreamExt;

use super::create_gossipsub_swarm;
use super::CreatedPrefixNode;
use super::ScudBehaviour;

use crate::db::LocalDB;

#[derive(Clone)]
pub struct SwarmHandle {
    sender: Sender<(CreatedPrefixNode, IdentTopic)>,
    topic: IdentTopic,
    pub peer_addr: Multiaddr,
    pub peer_id: PeerId,
}

impl SwarmHandle {
    pub fn new(
        topic: IdentTopic,
        peer_addr: Multiaddr,
        peer_id: PeerId,
    ) -> (SwarmHandle, Receiver<(CreatedPrefixNode, IdentTopic)>) {
        let (sender, receiver) = mpsc::channel(100);
        (
            SwarmHandle {
                sender,
                topic,
                peer_addr,
                peer_id,
            },
            receiver,
        )
    }

    pub async fn publish(&self, prefix: String, address: SocketAddr, version: u32) {
        //let capacity = self.sender.capacity();
        //println!("swarm channel capacity: {}", capacity);
        self.sender
            .send((
                CreatedPrefixNode {
                    prefix,
                    address,
                    version,
                },
                self.topic.clone(),
            ))
            .await
            .unwrap();
    }
}

async fn run_swarm(
    mut swarm: Swarm<ScudBehaviour>,
    mut receiver: Receiver<(CreatedPrefixNode, IdentTopic)>,
) {
    let mut listening = false;
    loop {
        tokio::select! {
            sibling = receiver.recv() => {
                let (sibling, topic) = sibling.unwrap();
                //println!("publishing sibling: {:?}", sibling);
                match swarm.behaviour_mut().gossipsub.publish(topic, serialize(&sibling).unwrap()) {
                    Ok(message_id) => {}, //println!("sucessfully published message {}", message_id),
                    Err(publish_error) => println!("failed publishing sibling with error {}", publish_error)
                };
            }
            event = swarm.select_next_some() => {dbg!(event);}
        }
        if !listening {
            for addr in swarm.listeners() {
                println!("Listening on {:?}", addr);
                listening = true;
            }
        }
    }
}

pub async fn create_swarm(
    receiver: Receiver<(CreatedPrefixNode, IdentTopic)>,
    peer_addrs: Vec<(Multiaddr, PeerId)>,
    localdb: LocalDB,
    listen_port: u16,
    topic: &IdentTopic,
    id_keys: Keypair,
) {
    let listen_multiaddr = create_multiaddr("0.0.0.0".parse::<IpAddr>().unwrap(), listen_port);
    let swarm = create_gossipsub_swarm(
        peer_addrs,
        listen_multiaddr,
        localdb.clone(),
        topic,
        id_keys,
    )
    .await
    .unwrap();
    tokio::spawn(run_swarm(swarm, receiver));
}

pub fn create_multiaddr(ip: IpAddr, port: u16) -> Multiaddr {
    let mut peer_addr: Multiaddr = ip.into();
    peer_addr.push(Protocol::Tcp(port));
    peer_addr
}
