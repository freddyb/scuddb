use bincode::deserialize;
use libp2p::{
    core::{upgrade, PublicKey},
    gossipsub::{
        self, Gossipsub, GossipsubEvent, GossipsubMessage, IdentTopic, MessageAuthenticity,
        MessageId, ValidationMode,
    },
    identify::{Identify, IdentifyConfig, IdentifyEvent, IdentifyInfo},
    identity::{self, Keypair},
    kad::{
        self, record::store::MemoryStore, GetClosestPeersError, GetClosestPeersOk, Kademlia,
        KademliaConfig, KademliaEvent, QueryResult,
    },
    mplex,
    noise,
    swarm::{NetworkBehaviourEventProcess, SwarmBuilder},
    // `TokioTcpConfig` is available through the `tcp-tokio` feature.
    tcp::TokioTcpConfig,
    Multiaddr,
    NetworkBehaviour,
    PeerId,
    Swarm,
    Transport,
};
use serde::{Deserialize, Serialize};
use std::collections::hash_map::DefaultHasher;
use std::error::Error;
use std::hash::{Hash, Hasher};
use std::net::SocketAddr;
use std::time::Duration;

use crate::db::LocalDB;

pub mod swarmhandle;

#[derive(Serialize, Deserialize, Debug)]
pub struct CreatedPrefixNode {
    pub prefix: String,
    pub address: SocketAddr,
    //TODO: look into libp2p message validators to prevent older versions from getting forwarded
    pub version: u32,
}

// We create a custom network behaviour that combines gossipsub and mDNS.
// The derive generates a delegating `NetworkBehaviour` impl which in turn
// requires the implementations of `NetworkBehaviourEventProcess` for
// the events of each behaviour.
#[derive(NetworkBehaviour)]
#[behaviour(event_process = true)]
struct ScudBehaviour {
    gossipsub: Gossipsub,
    kademlia: Kademlia<MemoryStore>,
    identify: Identify,

    #[behaviour(ignore)]
    localdb: LocalDB,
    #[behaviour(ignore)]
    runtime_handle: tokio::runtime::Handle,
    #[behaviour(ignore)]
    bench_start_time: Option<std::time::Instant>,
}

impl NetworkBehaviourEventProcess<GossipsubEvent> for ScudBehaviour {
    fn inject_event(&mut self, event: GossipsubEvent) {
        if let GossipsubEvent::Message {
            message,
            message_id,
            propagation_source,
        } = event
        {
            let CreatedPrefixNode {
                prefix,
                address,
                version,
            } = deserialize(&message.data).unwrap();
            //println!(
            //    "Received prefix: '{}', address: {:?}, version: {} from {:?}",
            //    prefix, address, version, message.source
            //);
            //println!(
            //    "message id was {:?} and propagation_source was {:?}",
            //    message_id, propagation_source
            //);
            match prefix.as_str() {
                "/bolaba/" => {
                    self.bench_start_time = Some(std::time::Instant::now());
                },
                "/bolaba/end/" => {
                    println!("receiving prefixes took {} ms", self.bench_start_time.unwrap().elapsed().as_millis());
                },
                _ => {},
            }

            let newdb = self.localdb.clone();
            self.runtime_handle.spawn(async move {
                newdb.upsert_prefix_ref(&prefix, address).await.unwrap();
            });
        }
    }
}

impl NetworkBehaviourEventProcess<IdentifyEvent> for ScudBehaviour {
    fn inject_event(&mut self, event: IdentifyEvent) {
        match event {
            IdentifyEvent::Received {
                peer_id,
                info:
                    IdentifyInfo {
                        listen_addrs,
                        protocols,
                        ..
                    },
            } => {
                if protocols
                    .iter()
                    .any(|p| p.as_bytes() == kad::protocol::DEFAULT_PROTO_NAME)
                {
                    for addr in listen_addrs {
                        println!("received addr {addr} trough identify");
                        self.kademlia.add_address(&peer_id, addr);
                    }
                } else {
                    println!("something funky happened, investigate it");
                }
            }
            e => {
                println!("got other identify event");
                dbg!(e);
            }
        }
    }
}

impl NetworkBehaviourEventProcess<KademliaEvent> for ScudBehaviour {
    fn inject_event(&mut self, event: KademliaEvent) {
        match event {
            //TODO: also add address to gossipsub when kademlia routing table is updated
            //When would this happen though? -> It would happen when add_address is called trough
            //identify
            KademliaEvent::OutboundQueryCompleted {
                result: QueryResult::GetClosestPeers(result),
                ..
            } => {
                match result {
                    Ok(GetClosestPeersOk { key, peers }) => {
                        if !peers.is_empty() {
                            println!("Query finished with closest peers: {:#?}", peers);
                            for peer in peers {
                                println!("gossipsub adding peer {peer}");
                                self.gossipsub.add_explicit_peer(&peer);
                            }
                        } else {
                            println!("Query finished with no closest peers.")
                        }
                    }
                    Err(GetClosestPeersError::Timeout { peers, .. }) => {
                        if !peers.is_empty() {
                            println!("Query timed out with closest peers: {:#?}", peers);
                            for peer in peers {
                                println!("gossipsub adding peer {peer}");
                                self.gossipsub.add_explicit_peer(&peer);
                            }
                        } else {
                            println!("Query timed out with no closest peers.");
                        }
                    }
                };
            }
            evnt => {
                dbg!(evnt);
            }
        }
    }
}

async fn create_gossipsub_swarm(
    peeraddrs: Vec<(Multiaddr, PeerId)>,
    listen_addr: Multiaddr,
    localdb: LocalDB,
    topic: &IdentTopic,
    id_keys: Keypair,
) -> Result<Swarm<ScudBehaviour>, Box<dyn Error>> {
    env_logger::init();

    let local_peer_id = PeerId::from(id_keys.public());
    println!("Local peer id: {:?}", local_peer_id);

    // Create a keypair for authenticated encryption of the transport.
    let noise_keys = noise::Keypair::<noise::X25519Spec>::new()
        .into_authentic(&id_keys)
        .expect("Signing libp2p-noise static DH keypair failed.");

    // Create a tokio-based TCP transport use noise for authenticated
    // encryption and Mplex for multiplexing of substreams on a TCP stream.
    let transport = TokioTcpConfig::new()
        .nodelay(true)
        .upgrade(upgrade::Version::V1)
        .authenticate(noise::NoiseConfig::xx(noise_keys).into_authenticated())
        .multiplex(mplex::MplexConfig::new())
        .boxed();

    let runtime_handle = tokio::runtime::Handle::current();

    // Create a Swarm to manage peers and events.
    let mut swarm = {
        // To content-address message, we can take the hash of message and use it as an ID.
        let scudbehaviour = ScudBehaviour {
            gossipsub: create_gossipsub_behavior(id_keys.clone()),
            kademlia: create_kademlia_behavior(local_peer_id),
            identify: create_identify_behavior(id_keys.public()),
            localdb,
            runtime_handle,
            bench_start_time: None,
        };

        SwarmBuilder::new(transport, scudbehaviour, local_peer_id)
            // We want the connection background tasks to be spawned
            // onto the tokio runtime.
            .executor(Box::new(|fut| {
                tokio::spawn(fut);
            }))
            .build()
    };

    // subscribes to our topic
    swarm.behaviour_mut().gossipsub.subscribe(topic).unwrap();

    // Reach out to another node if specified
    for (peer_addr, peer_id) in peeraddrs {
        println!("dialing peer {}", peer_addr);
        swarm.dial(peer_addr.clone())?;
        println!("adding initial peer addr to kademli: routing table: {peer_addr}");
        // Add the bootnodes to the local routing table.
        swarm
            .behaviour_mut()
            .kademlia
            .add_address(&peer_id, peer_addr.clone());
        //doesn't seem to be completely necessary
        swarm
            .behaviour_mut()
            .identify
            .push(std::iter::once(peer_id));
    }

    // Order Kademlia to search for peers.
    let to_search: PeerId = identity::Keypair::generate_ed25519().public().into();
    println!("Searching for the closest peers to {:?}", to_search);
    swarm.behaviour_mut().kademlia.get_closest_peers(to_search);

    // Listen on all interfaces and whatever port the OS assigns
    swarm.listen_on(listen_addr)?;

    Ok(swarm)
}

fn create_gossipsub_behavior(id_keys: Keypair) -> Gossipsub {
    // To content-address message, we can take the hash of message and use it as an ID.
    let message_id_fn = |message: &GossipsubMessage| {
        let mut s = DefaultHasher::new();
        message.data.hash(&mut s);
        MessageId::from(s.finish().to_string())
    };

    // Set a custom gossipsub
    let gossipsub_config = gossipsub::GossipsubConfigBuilder::default()
        .heartbeat_interval(Duration::from_secs(10)) // This is set to aid debugging by not cluttering the log space
        .validation_mode(ValidationMode::Strict) // This sets the kind of message validation. The default is Strict (enforce message signing)
        .message_id_fn(message_id_fn) // content-address messages. No two messages of the
        .do_px() //does nothing, peer exchange is currently unimplemented in rust-libp2p
        // same content will be propagated.
        .build()
        .expect("Valid config");
    gossipsub::Gossipsub::new(MessageAuthenticity::Signed(id_keys), gossipsub_config)
        .expect("Correct configuration")
}

fn create_kademlia_behavior(local_peer_id: PeerId) -> Kademlia<MemoryStore> {
    // Create a Kademlia behaviour.
    let mut cfg = KademliaConfig::default();
    cfg.set_query_timeout(Duration::from_secs(5 * 60));
    let store = MemoryStore::new(local_peer_id);
    Kademlia::with_config(local_peer_id, store, cfg)
}

fn create_identify_behavior(local_public_key: PublicKey) -> Identify {
    Identify::new(IdentifyConfig::new("/ipfs/1.0.0".into(), local_public_key))
}
