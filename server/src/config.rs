use anyhow::Result;
use serde::Deserialize;
use std::fs;

#[derive(Deserialize, Debug, Clone)]
pub struct Config {
    pub host_address: String,
    pub public_address: String,
    pub location: String,
    pub siblings: Vec<String>,
    pub is_root: Option<bool>,
    pub testing: Option<bool>,
}

pub fn get_config(path: &str) -> Result<Config> {
    Ok(toml::from_str(&fs::read_to_string(path).unwrap())?)
}
