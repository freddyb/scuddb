pub mod config;
mod db;
mod metacom;
mod node;
mod server;

use std::net::SocketAddr;
use std::net::ToSocketAddrs;

pub fn run_servers(config: config::Config) {
    let siblings: Vec<SocketAddr> = (&config.siblings)
        .iter()
        .map(|s| get_socket_addr(s))
        .collect();
    let public_address = get_socket_addr(&config.public_address);
    let host_address = get_socket_addr(&config.host_address);

    let rt = tokio::runtime::Runtime::new().unwrap();
    rt.block_on(async {
        //console_subscriber::ConsoleLayer::builder()
        //    .retention(Duration::from_secs(60))
        //    .server_addr(([127, 0, 0, 1], public_address.port() + 3))
        //    .init();
        let db = db::LocalDB::new(
            public_address,
            &config.location,
            &siblings,
            config.is_root.unwrap_or(false),
            config.testing.unwrap_or(false),
        )
        .await;
        let tarpchandle = tokio::spawn(server::tarpc::run_server(
            siblings,
            host_address,
            db.clone(),
        ));
        let grpchandle = tokio::spawn(server::grpc::run_server(host_address, db));
        tarpchandle.await.unwrap();
        grpchandle.await.unwrap();
    });
}

pub fn get_socket_addr(string_addr: &str) -> SocketAddr {
    string_addr.to_socket_addrs().unwrap().next().unwrap()
}
