use tonic::{transport::Server, Request, Response, Status};

use grpc::dbapi::db_api_server::{DbApi, DbApiServer};
use grpc::dbapi::{Empty, GetReply, GetRequest, UpsertRequest};

use std::net::SocketAddr;
//use libp2p::core::multiaddr::Protocol;
//use libp2p::Multiaddr;

use crate::db::LocalDB;

#[derive(Debug, Clone)]
struct DbServer {
    db: LocalDB,
}

#[tonic::async_trait]
impl DbApi for DbServer {
    async fn get(
        &self,
        request: Request<GetRequest>, // Accept request of type HelloRequest
    ) -> Result<Response<GetReply>, Status> {
        let key = request.into_inner().key;
        let node = self.db.get_node(&key)?.0;

        let reply = grpc::dbapi::GetReply {
            value: node.get(&key).await.unwrap().unwrap(), // We must use .into_inner() as the fields of gRPC requests and responses are private
        };

        Ok(Response::new(reply)) // Send back our formatted greeting
    }

    async fn upsert(
        &self,
        request: Request<UpsertRequest>, // Accept request of type HelloRequest
    ) -> Result<Response<Empty>, Status> {
        let request = request.into_inner();

        let key = request.key;
        let value = request.value;

        self.db.get_node(&key)?.0.upsert(key, value).await.unwrap();

        let reply = grpc::dbapi::Empty {};

        Ok(Response::new(reply)) // Send back our formatted greeting
    }
}

//#[tokio::main]
pub async fn run_server(host_address: SocketAddr, db: LocalDB) {
    //TODO: better adress generation, this leads to conflicts when running multiple nodes on the
    //same machine
    let addr = SocketAddr::new(host_address.ip(), host_address.port() + 1);
    println!("grpc listening on {}", addr);
    let db_api_server = DbServer { db };

    Server::builder()
        .add_service(DbApiServer::new(db_api_server))
        .serve(addr)
        .await
        .unwrap();
}
