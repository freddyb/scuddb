use crate::metacom::swarmhandle::{self, SwarmHandle};
use crate::node::{connection_pool::ConnectionPool, noderef::NodeRef, Node, NodeRepr};
use bincode::{deserialize, serialize};
use lazy_static::lazy_static;
use regex::Regex;
use rpc::{PrefixNode, ScudError};
use sled::{transaction::TransactionResult, Db, Tree};

use libp2p::gossipsub::IdentTopic;
use libp2p::identity::Keypair;
use libp2p::Multiaddr;
use libp2p::PeerId;

use rand::RngCore;
use std::net::SocketAddr;

#[derive(Clone)]
pub struct LocalDB {
    db: Db,
    pretree: Tree,
    pub public_address: SocketAddr,
    swarm_handle: SwarmHandle,
    pub connection_pool: ConnectionPool,
}

impl LocalDB {
    pub async fn new(
        public_address: SocketAddr,
        location: &str,
        siblings: &[SocketAddr],
        is_root: bool,
        testing: bool,
    ) -> LocalDB {
        //create local sled database
        let sled_config = sled::Config::default();
        let db = if testing {
            sled_config.temporary(true)
        } else {
            sled_config.path(location)
        }
        .open()
        .unwrap();
        let pretree = db.open_tree(b"prefix").unwrap();

        // Create a random local Keypair, this is done here so that the swarmhandle can be created with
        // the peerId in it. Querying the swarm afterwards would be possible, but harder to implement.
        let id_keys = Keypair::generate_ed25519();
        //generate address to return on multiaddr queries from other nodes
        let public_multiaddr =
            swarmhandle::create_multiaddr(public_address.ip(), public_address.port() + 2);
        let topic = IdentTopic::new("siblingkeys");

        //create swarm handle, this is done seperatly from the swarm creation because of the
        //dependency circle: swarm -> localdb -> swarm_handle -> (trough mpsc) swarm
        //this circle is resolved with the use of a mpsc channel, but it still means the handle and
        //localdb need to be created before the swarm itself can be spawned
        let (swarm_handle, receiver) = SwarmHandle::new(
            topic.clone(),
            public_multiaddr.clone(),
            PeerId::from(id_keys.public()),
        );
        let connection_pool = ConnectionPool::new();
        let localdb = LocalDB {
            db,
            pretree,
            public_address,
            swarm_handle,
            connection_pool,
        };
        let sibling_peer_addr = get_sibling_peer_addr(siblings, &localdb.connection_pool).await;
        swarmhandle::create_swarm(
            receiver,
            sibling_peer_addr,
            localdb.clone(),
            public_address.port() + 2,
            &topic,
            id_keys,
        )
        .await;

        if is_root {
            localdb
                .pretree
                .insert("/".as_bytes(), serialize(&NodeRepr::Local).unwrap())
                .unwrap();
            localdb.db.flush_async().await.unwrap();
        }

        add_siblings(siblings, localdb.clone(), is_root).await;

        localdb
    }

    //gets node associated with prefix when key ending with / is passed
    //example: /hi/hello/ returns the node associated with /hi/hello/
    //but /hi/hello returns the node associated with /hi/
    //TODO: perhaps we should only return the node with the next up prefix
    //For example: AA/BB/CC -> AA/BB/ and error if only AA exists but not AA/BB
    //At the very least we need a system to avoid writing to AA/BB/CC when AA/BB doesn't exist
    //since the implications of what this means for simultaneous creation of prefixes is not
    //well understood. It might also imply problems for moving prefixes.
    //Examine if this should be handled here, or in the create_prefix and insert implementations
    pub fn get_node(&self, key: &str) -> Result<(Node, String), ScudError> {
        lazy_static! {
            static ref RE: Regex = Regex::new("(^.*/)").unwrap();
        }
        if !key.starts_with('/') {
            return Err(ScudError::InvalidPrefix(key.to_string()));
        }

        let mut key_end = RE.find(key).unwrap().end();
        while !&self
            .pretree
            .contains_key(key[0..key_end].as_bytes())
            .unwrap()
        {
            //println!("{}", &key[0..key_end]);

            if let Some(mat) = RE.find(key[0..key_end].strip_suffix('/').unwrap()) {
                key_end = mat.end();
            } else {
                return Err(ScudError::OperationFailed);
            };
        }

        let prefix = &key[0..key_end];

        let node_bytes = &self.pretree.get(prefix.as_bytes()).unwrap().unwrap();
        Ok((Node::deserialize(node_bytes, self), prefix.to_string()))
    }

    pub fn get_prefixes(&self) -> Result<Vec<PrefixNode>, ScudError> {
        self.pretree
            .iter()
            .map(|res| {
                Ok(match res {
                    Ok((kb, vb)) => {
                        let prefix: String = std::str::from_utf8(&kb).unwrap().to_string();
                        let address: SocketAddr = Node::deserialize(&vb, self).get_address();
                        PrefixNode {
                            prefix,
                            address,
                            version: rand::rngs::OsRng.next_u32(),
                        }
                    }
                    _ => panic!(),
                })
            })
            .collect()
    }

    //TODO: wrap get & insert methods in transactions to deal with the owner of a key changing
    //midway through the request. (Not necessary currently as key moving isn't implemented)
    pub fn get(&self, key: &str) -> Result<Option<String>, ScudError> {
        //println!("getting key {key}");
        if let Some(ivec) = &self.db.get(key).unwrap() {
            Ok(Some(deserialize(ivec).unwrap()))
        } else {
            Ok(None)
        }
    }

    pub fn get_multiple(&self, keys: &[String]) -> Result<Vec<Option<String>>, ScudError> {
        //TODO: this should probably be abstracted into a function as other batch operations might
        // need this functionality
        // This checks if all requested keys fall under the same prefix
        // Perhaps we should also allow request that all go to the same node
        let first_prefix = self.get_node(keys.first().unwrap())?.1;
        for key in keys {
            if self.get_node(key)?.1 != first_prefix {
                return Err(ScudError::InvalidPrefix(String::from(
                    "keys must be owned by the same prefix",
                )));
            }
        }
        let transaction_result: TransactionResult<_, ScudError> = self.db.transaction(|db| {
            Ok(keys
                .iter()
                .map(|key| db.get(key).unwrap().map(|ivec| deserialize(&ivec).unwrap()))
                .collect())
        });

        Ok(transaction_result.unwrap())
    }

    pub fn get_p2p_info(&self) -> (Multiaddr, PeerId) {
        (
            self.swarm_handle.peer_addr.clone(),
            self.swarm_handle.peer_id,
        )
    }

    //for now assume inserting cant fail
    pub async fn upsert(&self, key: String, value: String) -> Result<(), ScudError> {
        println!("upserting key {} with value {}", key, value);
        self.db
            .insert(key.as_bytes(), serialize(&value).unwrap())
            .unwrap();
        //println!("pre_flush upsert");
        self.db.flush_async().await.unwrap();
        //println!("post flush upsert");
        Ok(())
    }

    pub async fn compare_and_swap(
        &self,
        key: String,
        old: Option<String>,
        new: Option<String>,
    ) -> Result<(), ScudError> {
        let old = old.map(|o| serialize(&o).unwrap());
        let new = new.map(|n| serialize(&n).unwrap());
        match self.db.compare_and_swap(key.as_bytes(), old, new).unwrap() {
            Ok(_) => Ok(()),
            Err(e) => Err(e.into()),
        }
    }

    ///for locally adding a prefix
    //TODO: we probably shouldn't be able to add a prefix if:
    //a more specific prefix already exists
    //or even if a more specific value already exists? -> theorethically possible to add prefix
    //anyways if the value is on the current node, but we won't allow it in this implementation
    //on the other hand, this can be prevented by requiring a prefix to be explicitly created for each
    //prefix level. For example, if you wanted to write to a/b/c/d, you'd first have to
    //individually create the prefixes a/b/c/. Or perhaps creating the prefix a/b/c could
    //automatically create a/b/ if those prefixes didn't exist yet.
    //Automatic prefix creation might be undesirable for applications who regularly want to create
    //deep prefixes owned by a single node, e.g. a deep prefix subtree under /users/bob. Although I
    //don't see any immediately negative effects of automatic prefix creation, except that moving
    //all prefixes under /users/bob would then become a bit more complicated. This complication
    //could be adressed by offering functionality to move all prefixes under a certain prefix,
    //instead of just the single prefix. This should be easy when all sub prefixes are stored on
    //the same node, but still manageable when they're spread on other nodes, since a node should
    //always be knowledgeable about all immediate sub-prefixes of its own prefixes.
    //TODO: wrap in a transaction to ensure that the same prefix doesn't get created twice, or that a
    //conflicting more general or conflicting more specific prefix doesn't get created at the same
    //time.
    pub async fn create_prefix(self, prefix: &str) -> Result<(), ScudError> {
        //This check is useful to ensure there is no confusion whether the /a or /a/b/ prefix gets
        //created when creating /a/b instead of /a/b/. This confusion may arise because get_node
        //returns the node with /a/ for /a/b, however this behavior isn't directly visible to the
        //user.
        if !prefix.ends_with('/') {
            return Err(ScudError::InvalidPrefix(String::from(prefix)));
        }
        //no need to strip / of the end because get_node doesn't
        //know of the existence of this prefix yet, so it will do it by itself
        let parent = self.get_node(prefix)?.0;

        //This match is here, instead of in the node impl like matches with other methods because a different
        //action needs to be taken in the node::Local case depending on where the request originates.
        //If the parent is the local node, nothing should be done upon prefix creation, but when
        //the parent receives a request_prefix_creation request, the relevant node is now local
        //from the parents perspective, where it should call db.request_prefix_creation instead of
        //doing nothing.
        //Simply put, matching on a node is for matching on the node which needs to actually handle
        //the request, while we need to match here first to determine if a request should be sent
        //in the first place.
        match parent {
            Node::Local(_) => self.check_subprefix_creation(prefix)?,
            Node::Remote(noderef) => {
                noderef
                    .request_prefix_creation(prefix, self.public_address)
                    .await?;
            }
        };

        //TODO: use upsert_prefix_ref here instead of duplicating the code for this insertion
        self.pretree
            .insert(prefix.as_bytes(), serialize(&NodeRepr::Local).unwrap())
            .unwrap();
        self.pretree.flush_async().await.unwrap();
        self.swarm_handle
            .publish(
                prefix.to_string(),
                self.public_address,
                rand::rngs::OsRng.next_u32(),
            )
            .await;
        Ok(())
    }

    ///Used by a subnode when requesting the creation of a sub-prefix.
    ///Upserting a prefix ref is not allowed if another sub-prefix or value
    ///already exists.
    pub async fn request_prefix_creation(
        &self,
        prefix: &str,
        address: SocketAddr,
    ) -> Result<(), ScudError> {
        println!(
            "got prefix creation request for prefix {} with addr {}",
            prefix, address
        );
        self.check_subprefix_creation(prefix)?;
        self.upsert_prefix_ref(prefix, address).await
    }

    fn check_subprefix_creation(&self, prefix: &str) -> Result<(), ScudError> {
        if let Some(subprefix) = self.pretree.scan_prefix(prefix.as_bytes()).next() {
            return Err(ScudError::SubPrefixExists(
                prefix.to_string(),
                String::from_utf8_lossy(&subprefix.unwrap().0).to_string(),
            ));
        };
        if let Some(subkey) = self.db.scan_prefix(prefix.as_bytes()).next() {
            return Err(ScudError::SubKeyExists(
                prefix.to_string(),
                String::from_utf8_lossy(&subkey.unwrap().0).to_string(),
            ));
        };
        Ok(())
    }

    ///For upserting prefix references to other nodes.
    ///This method can be called after a new prefix is discovered through libp2p,
    ///or after the creation of sub-prefix is requested through this node.
    ///This method is also used to populate the prefix metadata store on node startup.
    pub async fn upsert_prefix_ref(
        &self,
        prefix: &str,
        address: SocketAddr,
    ) -> Result<(), ScudError> {
        //println!(
        //    "upserting prefix_ref {} with addr {} at time {:?}",
        //    prefix,
        //    address,
        //    std::time::SystemTime::now()
        //);
        assert!(prefix.ends_with('/'));
        self.pretree
            .insert(
                prefix.as_bytes(),
                Node::Remote(NodeRef::new(address, &self.connection_pool)).serialize(),
            )
            .unwrap();
        self.pretree.flush_async().await.unwrap();
        Ok(())
    }
}

impl std::fmt::Debug for LocalDB {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("LocalDB")
            .field("db", &self.db)
            .field("pretree", &self.pretree)
            .field("public_address", &self.public_address)
            .finish()
    }
}

async fn add_siblings(siblings: &[SocketAddr], localdb: LocalDB, is_root: bool) {
    if is_root {
        for &addr in siblings {
            //TODO: add step to add peer adresses for libp2p
            //Not necessary, lip2p will do its own peer discovery
            let sibling_ref = NodeRef::new(addr, &localdb.connection_pool);
            match sibling_ref.get_prefixes().await {
                Ok(prefixlist) => {
                    for prefix_node in prefixlist {
                        localdb
                            .upsert_prefix_ref(&prefix_node.prefix, prefix_node.address)
                            .await
                            .unwrap();
                    }
                }
                _ => panic!(), //panic when we don't get a prefix list for now
            };
        }
    }
    for &addr in siblings {
        let sibling_ref = NodeRef::new(addr, &localdb.connection_pool);
        tokio::join!(add_sibling_prefixes(sibling_ref, &localdb));
        //add_sibling_peer_addr(sibling_ref, &localdb.swarm_handle));
    }
}

async fn add_sibling_prefixes(sibling_ref: NodeRef<'_>, localdb: &LocalDB) {
    match sibling_ref.get_prefixes().await {
        Ok(prefixlist) => {
            for prefix_node in prefixlist {
                localdb
                    .upsert_prefix_ref(&prefix_node.prefix, prefix_node.address)
                    .await
                    .unwrap();
            }
        }
        _ => panic!(), //panic when we don't get a prefix list for now
    };
}

use futures::future;

async fn get_sibling_peer_addr(
    siblings: &[SocketAddr],
    connection_pool: &ConnectionPool,
) -> Vec<(Multiaddr, PeerId)> {
    future::try_join_all(
        siblings
            .iter()
            .map(|addr| async { NodeRef::new(*addr, connection_pool).get_p2p_info().await }),
    )
    .await
    .unwrap()
}
