pub mod connection_pool;
pub mod noderef;

use crate::db::LocalDB;
use bincode::{deserialize, serialize};
use serde::{Deserialize, Serialize};
use std::net::SocketAddr;

use noderef::NodeRef;
use rpc::ScudError;

//exclusively used for serialization
#[derive(Serialize, Deserialize, Debug)]
pub enum NodeRepr {
    Local,
    Remote(SocketAddr),
}

//TODO: somehow add leaf/not a leaf property to these things
//Should we prevent writes to non leaf nodes?
//Maybe we can implicitly limit writes to only leaf nodes trough modifying the get_node behavior
#[derive(Debug)]
pub enum Node<'a> {
    Local(&'a LocalDB),
    Remote(NodeRef<'a>),
}

impl<'a> Node<'_> {
    pub fn deserialize(node_bytes: &[u8], db: &'a LocalDB) -> Node<'a> {
        match deserialize::<NodeRepr>(node_bytes).unwrap() {
            NodeRepr::Local => Node::Local(db),
            NodeRepr::Remote(addr) => Node::Remote(NodeRef::new(addr, &db.connection_pool)),
        }
    }
    pub fn serialize(&self) -> Vec<u8> {
        match self {
            Node::Local(_) => serialize(&NodeRepr::Local).unwrap(),
            Node::Remote(noderef) => serialize(&NodeRepr::Remote(noderef.addr)).unwrap(),
        }
    }

    pub fn get_address(&self) -> SocketAddr {
        match self {
            Node::Local(db) => db.public_address,
            Node::Remote(noderef) => noderef.addr,
        }
    }

    pub async fn get(&self, key: &str) -> Result<Option<String>, ScudError> {
        match self {
            Node::Local(db) => db.get(key),
            Node::Remote(noderef) => noderef.get(key).await,
        }
    }

    pub async fn get_multiple(&self, keys: &Vec<String>) -> Result<Vec<Option<String>>, ScudError> {
        match self {
            Node::Local(db) => db.get_multiple(keys),
            Node::Remote(noderef) => noderef.get_multiple(keys).await,
        }
    }

    pub async fn upsert(&self, key: String, value: String) -> Result<(), ScudError> {
        match self {
            Node::Local(db) => db.upsert(key, value).await,
            Node::Remote(noderef) => noderef.upsert(key, value).await,
        }
    }

    pub async fn compare_and_swap(
        &self,
        key: String,
        old: Option<String>,
        new: Option<String>,
    ) -> Result<(), ScudError> {
        match self {
            Node::Local(db) => db.compare_and_swap(key, old, new).await,
            Node::Remote(noderef) => noderef.compare_and_swap(key, old, new).await,
        }
    }

    pub async fn upsert_prefix_ref(&self, prefix: &str, addr: SocketAddr) -> Result<(), ScudError> {
        match self {
            Node::Local(db) => db.upsert_prefix_ref(prefix, addr).await,
            Node::Remote(noderef) => noderef.upsert_prefix_ref(prefix, addr).await,
        }
    }

    pub async fn request_prefix_creation(
        &self,
        prefix: &str,
        addr: SocketAddr,
    ) -> Result<(), ScudError> {
        match self {
            Node::Local(db) => db.request_prefix_creation(prefix, addr).await,
            Node::Remote(noderef) => noderef.request_prefix_creation(prefix, addr).await,
        }
    }
}
