use super::connection_pool::ConnectionPool;
use std::net::SocketAddr;

use rpc::{PrefixNode, ScudError, ScudProtocolClient};
use tarpc::context;

use libp2p::Multiaddr;
use libp2p::PeerId;

//TODO: is it safe to only put version info on noderefs and its serialized equivalent,
//NodeRepr::Remote?
//I would think so, because when we have local authority, we're always certain we have the latest
//version. -> WRONG!!! I do need to store it in both local & remote refs to preserve an ever
//increasing version counter for future moves.
//On adding this version info as a separate arg, vs embedding it into a struct together with a
//socektaddr (kind of like what NodeRepr::Remote is already, both may be acceptable, since
//upsert_prefix ref is only called between database nodes, and not by clients. Therefore higher api
//friction is acceptable.
#[derive(Copy, Clone, Debug)]
pub struct NodeRef<'a> {
    pub addr: SocketAddr,
    connection_pool: &'a ConnectionPool,
}

impl NodeRef<'_> {
    pub fn new(addr: SocketAddr, connection_pool: &ConnectionPool) -> NodeRef<'_> {
        NodeRef {
            addr,
            connection_pool,
        }
    }

    async fn get_client(&self) -> ScudProtocolClient {
        self.connection_pool.get(self.addr).await
    }

    //TODO: Look into abstracting these function bodies into a single generic function, since they all
    //follow the same structure. Might be possible with macros, but that's probably too complicated
    //for this use case.
    pub async fn get(&self, key: &str) -> Result<Option<String>, ScudError> {
        //println!("getting from remote node");
        self.get_client()
            .await
            .get(context::current(), key.to_owned())
            .await
            .unwrap()
    }

    pub async fn get_multiple(&self, keys: &Vec<String>) -> Result<Vec<Option<String>>, ScudError> {
        self.get_client()
            .await
            .get_multiple(context::current(), keys.to_owned())
            .await
            .unwrap()
    }

    pub async fn upsert(&self, key: String, value: String) -> Result<(), ScudError> {
        self.get_client()
            .await
            .upsert(context::current(), key, value)
            .await
            .unwrap()
    }

    pub async fn compare_and_swap(
        &self,
        key: String,
        old: Option<String>,
        new: Option<String>,
    ) -> Result<(), ScudError> {
        self.get_client()
            .await
            .compare_and_swap(context::current(), key, old, new)
            .await
            .unwrap()
    }

    pub async fn request_prefix_creation(
        &self,
        prefix: &str,
        addr: SocketAddr,
    ) -> Result<(), ScudError> {
        self.get_client()
            .await
            .request_prefix_creation(context::current(), prefix.to_owned(), addr)
            .await
            .unwrap()
    }

    pub async fn upsert_prefix_ref(&self, prefix: &str, addr: SocketAddr) -> Result<(), ScudError> {
        self.get_client()
            .await
            .upsert_prefix_ref(context::current(), prefix.to_owned(), addr)
            .await
            .unwrap()
    }

    pub async fn get_prefixes(&self) -> Result<Vec<PrefixNode>, ScudError> {
        self.get_client()
            .await
            .get_prefixes(context::current())
            .await
            .unwrap()
    }

    pub async fn get_p2p_info(&self) -> Result<(Multiaddr, PeerId), ScudError> {
        self.get_client()
            .await
            .get_p2p_info(context::current())
            .await
            .unwrap()
    }
}
