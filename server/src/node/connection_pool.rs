use dashmap::DashMap;
use std::net::SocketAddr;
use std::sync::Arc;

use rpc::ScudProtocolClient;
use tarpc::client;
use tarpc::tokio_serde::formats::Json;

#[derive(Clone, Debug)]
pub struct ConnectionPool {
    connection_map: Arc<DashMap<SocketAddr, ScudProtocolClient>>,
}

impl<'a> ConnectionPool {
    pub fn new() -> ConnectionPool {
        ConnectionPool {
            connection_map: Arc::new(DashMap::new()),
        }
    }
    pub async fn get(&self, addr: SocketAddr) -> ScudProtocolClient {
        match self.connection_map.get(&addr) {
            Some(valref) => valref.value().clone(),
            None => {
                let connection = ConnectionPool::create_connection(addr).await;
                self.connection_map.insert(addr, connection);
                self.connection_map.get(&addr).unwrap().value().clone()
            }
        }
    }

    async fn create_connection(addr: SocketAddr) -> ScudProtocolClient {
        let transport = tarpc::serde_transport::tcp::connect(addr, Json::default);
        rpc::ScudProtocolClient::new(client::Config::default(), transport.await.unwrap()).spawn()
    }
}
