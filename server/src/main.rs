use anyhow::Result;
use clap::{Arg, Command};

use server::config;

fn main() -> Result<()> {
    //println!("hiyahe");
    let args = Command::new("myDB server")
        .arg(
            Arg::new("config_file")
                .short('c')
                .help("location of config file")
                .takes_value(true)
                .required(true),
        )
        .get_matches();

    let config = config::get_config(
        args.value_of("config_file")
            .expect("invalid configuration file path"),
    )?;
    dbg!(&config);
    server::run_servers(config);
    Ok(())
}
