# ScudDB server

You can run the server with `cargo run -- -c localrootconfig.toml`
An additional node can be connected with `cargo run -- -c localconfig.toml`

## Structure of this repo

- src/server contains code for initial incoming requests. These requests are then delegated to a Node, an enum representing either the local node, or a remote one.
- src/node contains the implementation of the node enum. Request forwarding is also handled by the Node enum when necessary.
- src/metacom contains libp2p related code responsible for sharing prefix location metadata between nodes.
- src/db.rs represents a local database node which is responsible for writes to the embedded sled database.
- main.rs sets up the application, with the help of a config described in config.rs
