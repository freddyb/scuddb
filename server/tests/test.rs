use std::net::SocketAddr;

#[test]
fn test_get_socket_addr() {
    assert_eq!(
        server::get_socket_addr("1.2.3.4:5000"),
        "1.2.3.4:5000".parse::<SocketAddr>().unwrap()
    );
    let resolvedname = server::get_socket_addr("localhost:5000");
    assert!(
        resolvedname == "127.0.0.1:5000".parse().unwrap()
            || resolvedname == "[::1]:5000".parse().unwrap()
    );
}
